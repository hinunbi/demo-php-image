FROM centos:7.6.1810

EXPOSE 8090

# S2I setup
ENV STI_SCRIPTS_PATH=/usr/libexec/s2i \
    HOME=/home/fct \
    PATH=$PATH:/usr/local/apache2/bin \
    PHP_VERSION=5.6

LABEL io.k8s.description="Platform for building and running PHP 5.6(Tibero 5) applications" \
      io.k8s.display-name="Apache 2.4 with PHP 5.6(Tibero 5)" \
      io.openshift.s2i.scripts-url="image:///usr/libexec/s2i" \
      io.openshift.expose-services="8090:http" \
      io.openshift.tags="builder,centos,php,tibero,php56,tibero5"

# Labels consumed by Red Hat build service
LABEL Name="php-56-tibero-centos7" \
      BZComponent="php-tibero-docker" \
      Version="5.6" \
      Release="1.0" \
      Architecture="x86_64"

COPY .s2i/bin $STI_SCRIPTS_PATH

# Install prerequisite packages
RUN INSTALL_PKGS="libpng-devel libjpeg-devel freetype freetype-devel" && \
    yum install -y --setopt=tsflags=nodocs $INSTALL_PKGS && \
    yum clean all -y

# Install the provided Apache binary
ADD apache2.tar.gz /usr/local

# Tweak libexpat link for the provided Apache binary
RUN cd /usr/lib64 && \
    ln -s libexpat.so.1.6.0 libexpat.so.0

# Add execution user and web directory, requested by the provided Apache binary
RUN useradd -u 1001 -r -g 0 -d ${HOME} -s /sbin/nologin -c "Default Application User" default && \
    mkdir -p ${HOME} && \
    mkdir -p ${HOME}/www/ROOT && \
    mkdir -p ${HOME}/www/cgi-bin && \
    mkdir -p ${HOME}/www/SWC/CGI && \
    chown -R 1001:0 ${HOME} && chmod -R g+rwX ${HOME}

RUN chmod -R a+rwx /usr/local/apache2 && \
    chown -R 1001:0 /usr/local/apache2

RUN mkdir /tmp/sessions && \
    chmod -R a+rwx /tmp/sessions

# Install Tibero and and envrionment variables
COPY tibero5/tibero5-bin-FS05_CS_1902-linux64-166170-opt.tar.gz /home
RUN cd /home && \
    tar xvf tibero5-bin-FS05_CS_1902-linux64-166170-opt.tar.gz && \
    rm -f tibero5-bin-FS05_CS_1902-linux64-166170-opt.tar.gz
ENV TB_HOME=/home/tibero5
ENV TB_SID=tibero \
    PATH=$TB_HOME/bin:$TB_HOME/client/bin:$PATH \
    LD_LIBRARY_PATH=$TB_HOME/lib:$TB_HOME/client/lib:$LD_LIBRARY_PATH

RUN cd $TB_HOME/config && ./gen_tip.sh
RUN cd $TB_HOME/client/config && cat tbdsn.tbr
RUN chown -R 1001:0 $TB_HOME && chmod -R g+rwX $TB_HOME

# Overwrite Apache and PHP configuraton
ADD apache2/conf/httpd.conf /usr/local/apache2/conf
ADD apache2/conf/httpd-ssl.conf /usr/local/apache2/conf
ADD apache2/conf/php.ini /usr/local/apache2/conf

# uncomment for debuging
# RUN yum install -y net-tools
# ADD info.php ${HOME}/www/ROOT
# RUN chown -R 1001:0 ${HOME}/www/ROOT && \
#    chmod -R a+rwx ${HOME}/www/ROOT

USER 1001

# Set the default CMD to print the usage of the language image
CMD $STI_SCRIPTS_PATH/usage
